// Output.c

#include <stdio.h> //for sprintf function
#include "tft.h" //for tft specific functions
#include "TI_Lib.h" //for get_uptime();

#define ODR_MASK_PIN_4 (0x1U << 4)
#define ODR_MASK_PIN_5 (0x1U << 5)
#define ODR_MASK_PIN_6 (0x1U << 6)
#define ODR_MASK_PIN_7 (0x1U << 7)
#define ODR_MASK_PIN_6_AND_7 (0x3U << 6)
#define ODR_MASK_PIN_8_TO_15 (0xFFU << 8)
#define DELAY_RATE 500 // ms

volatile uint32_t timeD = 0;
volatile uint32_t prevTimeD = 0;

void displayCounterOnLEDs(int count) {
    uint8_t bitCount = count;
    GPIOG->ODR = GPIOG->IDR & ~ODR_MASK_PIN_8_TO_15;
    GPIOG->ODR = GPIOG->IDR | bitCount << 8;
}

void setDirectionalLED(int direction) {
    GPIOG->ODR = GPIOG->IDR & ~ODR_MASK_PIN_6_AND_7;
    if (direction == 1) { //forward
        GPIOG->ODR = GPIOG->IDR | ODR_MASK_PIN_7;
    }
    if (direction == 0) { //backwards
        GPIOG->ODR = GPIOG->IDR | ODR_MASK_PIN_6;
    }
}

void setErrorLED(int status) {
    if (status) { //turn it on
        GPIOG->ODR = GPIOG->IDR | ODR_MASK_PIN_5;
    } else { //tur  n it off
        GPIOG->ODR = GPIOG->IDR & ~ODR_MASK_PIN_5;
    }
}

void setDisplayControlLED(int status) {
    if (status) { //turn it on
        GPIOG->ODR = GPIOG->IDR | ODR_MASK_PIN_4;
    } else { //turn it off
        GPIOG->ODR = GPIOG->IDR & ~ODR_MASK_PIN_4;
    }
}

void putIntOnTFT(int value, int withDecimal) {
    char str[20];

    if (withDecimal) {
        int decimal = value % 10;
        if (decimal < 0) {
            decimal *= -1;
        }
        //sprintf(str, "%*s%d.%1d", -10 < value && value < 0, "-", value / 10, decimal);
        if (-10 < value && value < 0) {
            sprintf(str, "-%d.%1d", value / 10, decimal);
        } else {
            sprintf(str, "%d.%1d", value / 10, decimal); 
        }
    } else {
        sprintf(str, "%d", value);
    }
    TFT_puts(str);
}

void printData(int angle, int speed) {
    timeD = get_uptime();
    if (timeD - prevTimeD > DELAY_RATE) {
        setDisplayControlLED(1);

        char line0[15] = "Angle(deg)  : ";
        char line1[15] = "Speed(deg/s): ";

        TFT_cls();
        TFT_puts(line0);
        putIntOnTFT(angle, 1);
        TFT_newline();
        TFT_carriage_return();
        TFT_puts(line1);
        putIntOnTFT(speed, 1);
        prevTimeD = timeD;

        setDisplayControlLED(0);
    }
}
