// Control.c

#include "Output.h"
#include "Input.h"
#include "TI_Lib.h" //for get_uptime();

typedef enum {
    START = 0, phaseA, phaseB, phaseC, phaseD, phaseERROR
} phaseType;

// for speed calculation
volatile int angle = 0;
volatile int prevAngle = 0;
volatile int speed = 0;
volatile int prevSpeed = 0;
volatile int speedAverage = 0;

// input
volatile int p0;
volatile int p1;
volatile int s6;
volatile int s7;

volatile phaseType phase = START;
volatile phaseType prevPhase = START;

// to calculate phase changes
volatile int count;
volatile int prevCount;

volatile uint32_t time = 0;

//Functions-----------------------------------------

void phaseStart(void) {}

void phaseAaction(void) {
    if (prevPhase == phaseD) {
        prevCount = count;
        count--;
        setDirectionalLED(0);
    } else if (prevPhase == phaseC) {
        phase = phaseERROR;
        setErrorLED(1);
    } else if (prevPhase == phaseB) {
        prevCount = count;
        count++;
        setDirectionalLED(1);
    }
}

void phaseBaction(void) {
    if (prevPhase == phaseC) {
        prevCount = count;
        count++;
        setDirectionalLED(1);
    } else if (prevPhase == phaseA) {
        prevCount = count;
        count--;
        setDirectionalLED(0);
    } else if (prevPhase == phaseD) {
        phase = phaseERROR;
        setErrorLED(1);
    }
}

void phaseCaction(void) {
    if (prevPhase == phaseA) {
        phase = phaseERROR;
        setErrorLED(1);
    } else if (prevPhase == phaseD) {
        prevCount = count;
        count++;
        setDirectionalLED(1);
    } else if (prevPhase == phaseB) {
        prevCount = count;
        count--;
        setDirectionalLED(0);
    }
}

void phaseDaction(void) {
    if (prevPhase == phaseC) {
        prevCount = count;
        count--;
        setDirectionalLED(0);
    } else if (prevPhase == phaseB) {
        phase = phaseERROR;
        setErrorLED(1);
    } else if (prevPhase == phaseA) {
        prevCount = count;
        count++;
        setDirectionalLED(1);
    }
}

void phaseError(void) {}

void superloop(void) {
    printData(0, 0);
    void (*phaseTable[])(void) = {phaseStart, phaseAaction, phaseBaction, phaseCaction, phaseDaction,
                                  phaseError}; //function pointer array

    while (1) {
        p0 = getPIN0State();
        p1 = getPIN1State();
        s6 = getS6State();
        s7 = getS7State();

        if (s6) {
            setErrorLED(0);
        }
        if (s7) {
            angle = 0;
            prevAngle = 0;
            speed = 0;
            prevSpeed = 0;
            speedAverage = 0;
            phase = START;
            prevPhase = START;
            count = 0;
            prevCount = 0;
            time = 0;
            printData(0, 0);
            displayCounterOnLEDs(count);
            setErrorLED(0);
            setDirectionalLED(-1);
        }

        prevPhase = phase;
        if (p0 == 0 && p1 == 0) {
            phase = phaseA;
        } else if (p0 == 1 && p1 == 0) {
            phase = phaseB;
        } else if (p0 == 1 && p1 == 1) {
            phase = phaseC;
        } else if (p0 == 0 && p1 == 1) {
            phase = phaseD;
        } else {
            phase = phaseERROR;
        }

        phaseTable[phase]();

        // calculate angle
        angle = count * 3;

        // calculate speed
        if ((get_uptime() / 150) > time) {
            time = get_uptime() / 150;
            speed = (((angle - prevAngle) * 20) / 3);
            speedAverage = (speed + prevSpeed) / 2; // calculate average so number is smoother
            prevSpeed = speed;
            prevAngle = angle;
        }
        // update display
        printData(angle, speedAverage);
        displayCounterOnLEDs(count);
    }
}
