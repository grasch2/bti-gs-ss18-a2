/**
  ******************************************************************************
  * @file    main.c 
  * @author  Andrea Grabau
  * @author  Philipp Schwarz
  * @version V1.0
  * @date    2018-04-18
  * @brief   Main program body
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "TI_Lib.h"
#include "tft.h"
#include "Control.h"

//--- For GPIOs -----------------------------
//Include instead of "stm32f4xx.h" for
//compatibility between Simulation and Board
//#include "TI_memory_map.h"

//--- For Touch Keypad ----------------------
//#include "keypad.h"

//--- For Timer -----------------------------
#include "timer.h"

/**
  * @brief  Main program
  * @param  None
  */

int main(void) {
    Init_TI_Board();
    timerinit();
    TFT_Init();
    TFT_cls();
    superloop();
    return 0;
}
// EOF
