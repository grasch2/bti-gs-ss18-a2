// Input.h

/**
 * Returns 1 if PIN 0 is active and 0 if not
 */
int getPIN0State(void);

/**
  *Returns 1 if PIN 1 is active and 0 if not
 */
int getPIN1State(void);

/**
 * Returns 1 if S6 is pressed and 0 if not
 */
int getS6State(void);

/**
 * Returns 1 if S7 is pressed and 0 if not
 */
int getS7State(void);
