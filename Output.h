// Output.h

/*
 * Displays the last 8 digits of the counter
 * @param int count The counter
 */
void displayCounterOnLEDs(int count);

/*
 * Prints angle and speed onto the TFT
 * @param int angle The angle
 * @param int speed The speed
 */
void printData(int angle, int speed);

/*
 * Helper method for printData, puts an int on the tft
 * @param int value The value that is going to be printed
 * @param int withDecimal Control number, 1 means the lowest digit is a decimal place
 */
void putIntOnTFT(int value, int withDecimal);

/*
 * Sets the LED that shows the direction, D20 for forward, D19 for backwards
 * @param int direction 1 for forward, 0 for backward
 */
void setDirectionalLED(int direction);

/*
 * Sets the display output speed control LED D17
 * @param int status 1 for on, 0 for off
 */
void setDisplayControlLED(int status);

/*
 * Sets the error LED D16
 * @param int status 1 for on, 0 for off
 */
void setErrorLED(int status);
