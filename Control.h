// Control.h

/*
 *Modifies the program accordingly from the phase it comes from to phase A
 */
void phaseAaction(void);

/*
 *Modifies the program accordingly from the phase it comes from to phase B
 */
void phaseBaction(void);

/*
 *Modifies the program accordingly from the phase it comes from to phase C
 */
void phaseCaction(void);

/*
 *Modifies the program accordingly from the phase it comes from to phase D
 */
void phaseDaction(void);

/*
 * Only there for filling the phase function table, does nothing
 */
void phaseError(void);

/*
 * Only there for filling the phase function table, does nothing
 */
void phaseStart(void);

/*
 * The loop that gets the input accoding to DCC and controls the calculation and output of the program
 */
void superloop(void);
