// Input.c

#include "TI_memory_map.h"

#define IDR_MASK_PIN_0 (0x01U << 0)
#define IDR_MASK_PIN_1 (0x01U << 1)
#define IDR_MASK_PIN_6 (0x01U << 6)
#define IDR_MASK_PIN_7 (0x01U << 7)

int getPIN0State(void) {
    return (IDR_MASK_PIN_0 == (GPIOE->IDR & IDR_MASK_PIN_0));
}

int getPIN1State(void) {
    return (IDR_MASK_PIN_1 == (GPIOE->IDR & IDR_MASK_PIN_1));
}

int getS6State(void) {
    return (IDR_MASK_PIN_6 != (GPIOE->IDR & IDR_MASK_PIN_6));
}

int getS7State(void) {
    return (IDR_MASK_PIN_7 != (GPIOE->IDR & IDR_MASK_PIN_7));
}
